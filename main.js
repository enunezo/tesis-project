var core = require('./core');
var fs = require('fs');
var path = require('path');

// Setup vars
var outputFilePath = path.join(__dirname, 'demo/demo123.txt');
var jsonFile = path.join(__dirname,'demo/data.json');

function processFilesFromDoingDir () {

    var onFinish = function () {
        console.log(':D');
        // Move to done
        fs.readdirSync(path.join(__dirname, 'demo/doing')).forEach(function (entry) {
            fs.renameSync(
                path.join(__dirname, 'demo/doing', entry),
                path.join(__dirname, 'demo/done', entry)
            )
        });
        // Clean buff file
        fs.writeFileSync(outputFilePath, '');
        // Add more files to queue
        fs.readdirSync(path.join(__dirname, 'demo/toDo')).filter(function (entry, i) {
            return (i<1);
        }).forEach(function (entry) {
            fs.renameSync(
                path.join(__dirname, 'demo/toDo', entry),
                path.join(__dirname, 'demo/doing', entry)
            )
        });
        // Start again
        processFilesFromDoingDir();

    }

    // Get pdf entries list
    console.log('Reading entries')
    var entries = fs.readdirSync(path.join(__dirname, 'demo/doing')).filter(function (entryName) {
        return entryName.match(/\.pdf$/);
    }).map(function (entryName) {
        return path.join(__dirname, 'demo/doing', entryName);
    });

    if (!entries.length) {
        onFinish();
        return;
    }

    /* // Write pdfs content
    console.log('Extracting pdfs content');
    core.services.fromPdfsToFile(
        entries, outputFilePath
    ).then(function () {
        //Get Sentences
        console.log('Get Sentences');
        core.services.getSentences( outputFilePath, jsonFile ).then( function () { 
            // Clean file
            console.log('Cleaning content');
            core.services.cleanTextFromFile(outputFilePath, outputFilePath).then(function () {
                // From file to db
                console.log('Insering words into db');
                core.services.fromFileToDb(outputFilePath).then(function () {
                    console.log('Done');
                    onFinish();
                })
            })
        })
    }); */
    // Write pdfs content
    console.log('Extracting pdfs content');
    core.services.fromPdfsToFile(
        entries, outputFilePath
    ).then(function () {
        // Clean file
        console.log('Cleaning content');
        core.services.cleanTextFromFile(outputFilePath, outputFilePath).then(function () {
            // From file to db
            console.log('Insering words into db');
            core.services.fromFileToDb(outputFilePath).then(function () {
                console.log('Done');
                onFinish();
            })
        })
    });
    
}

processFilesFromDoingDir()

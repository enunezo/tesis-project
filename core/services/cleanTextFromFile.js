var stopWords = require('../data/stopWords.json');
var fs = require('fs');
var Q = require('q');
var stopWordsMap = {};
stopWords.forEach(function (stopWord) {
    stopWordsMap[stopWord] = true;
})

module.exports = function (inputFile, outputFile) {
    var deferred = Q.defer();
    var txt = fs.readFileSync(inputFile, 'utf-8');
    var cleanTxt = txt.toLocaleLowerCase().replace(/[^a-zA-Z\s]+/g, '');
    cleanTxt = cleanTxt.replace(/\s+/g, ' ');
    var splitTxt = cleanTxt.split(' ');
    var filterTxt = splitTxt.filter( function ( word ){
        return !stopWordsMap[word];
    });
    var r = filterTxt.join(' ');
    fs.writeFile(outputFile, r, function () {
        deferred.resolve();
    });
    return deferred.promise;
}
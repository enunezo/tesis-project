var fs = require('fs');
var path = require('path');
var Q = require('q');
var PDFParser = require("pdf2json");

module.exports = function (entriesList, outputFile) {
    var deferred = Q.defer();

    // Setup counter
    var counter = entriesList.length;
    var intval;

    // Init pdf parser
    var pdfParser = new PDFParser(this, 1);

    // Define event handlers
    pdfParser.on("pdfParser_dataError", errData => {
        console.error(errData);
        if (intval) { clearInterval(intval); }
        deferred.reject(errData);
    } );
    pdfParser.on("pdfParser_dataReady", pdfData => {
        if (!counter) return;
        counter--;
        console.log(outputFile, counter)
        fs.appendFileSync(outputFile, '\n');
        fs.appendFileSync(outputFile, pdfParser.getRawTextContent());
    });
    
    // Read entries
    entriesList.forEach(function(entryPath) {
        console.log(entryPath);
        pdfParser.loadPDF(entryPath);
    }, this);

    // Start counter watcher
    intval = setInterval(function () {
        if (counter <= 0) {
            clearInterval(intval)
            deferred.resolve(counter);
            return;
        }
    }, 1);

    return deferred.promise;
}
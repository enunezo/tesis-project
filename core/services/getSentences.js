var fs = require('fs');
var path = require('path');
var Q = require('q');
var Tokenizer = require('sentence-tokenizer');

var tokenizer = new Tokenizer('Chuck'); 


module.exports = function (inputFile, outputFile) {
    var deferred = Q.defer();
    var jsonContent;        
    var txt= '';
    fs.readFile(inputFile, 'utf-8', function( err, data) {
        if (err) throw err;
        tokenizer.setEntry( data );
        console.log(tokenizer.getSentences());
        jsonContent = tokenizer.getSentences();
        jsonContent = JSON.stringify( jsonContent );
        fs.appendFile ( outputFile, jsonContent, function(){
            deferred.resolve();
        });
    })

    
    return deferred.promise;
}



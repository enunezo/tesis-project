var MongoClient = require('mongodb').MongoClient , 
assert = require('assert');
var q = require('q');
var fs = require('fs');

// Connection URL
var url = 'mongodb://enunezo:enunezo@ds155695.mlab.com:55695/sentences-reduction'

// Use connect method to connect to the Server
function MongoModel (db, name){
    
    // Attributes
    this.collection = db.collection(name);
    
    // Methods
    this.insert = function(data){
        var deferred = q.defer();
        this.collection.insertOne(data, function(err, r){
            if (err) {
                deferred.reject(err);
                return;
            }
            deferred.resolve(r.ops[0]);
        });
        return deferred.promise;
    }
    
    this.delete = function(data){
        var deferred = q.defer();
        this.collection.deleteOne(data, function(err, r){
            if (err) {
                deferred.reject(err);
                return;
            }
            deferred.resolve(r);
        });
        return deferred.promise;
    }
    
    this.updateOne = function(criteria, modify){
        var deferred = q.defer();
        this.collection.updateOne(criteria, modify , function(err, r){
            if (err){
                deferred.reject(err);
                return;
            }
            deferred.resolve(r);   
        });
            return deferred.promise;
        }
        
    this.findOne = function(criteria){
        var deferred = q.defer();
        this.collection.findOne( criteria, function(err, r){
            if (err){
                deferred.reject(err);
                return;
            }
            deferred.resolve(r);
        });
        return deferred.promise;
    }
    
    this.findAll = function(criteria){
        var deferred = q.defer();
        this.collection.find( criteria, function(err, r){
            if (err){
                deferred.reject(err);
                return;
            }
            deferred.resolve(r);
        });
        return deferred.promise;
    }
    
}

module.exports = function (filePath) {
    var deferred = q.defer();

    // Connect to db
    MongoClient.connect(url, function(err, db) {
        if (err) {
            deferred.reject(err);
            return;
        }

        // Instance pair model
        var bigrams = new MongoModel(db,"pairs");

        // Read file
        var fileData = fs.readFileSync(filePath, 'utf-8');
        console.log(fileData.length)

        // Extract words
        var words = fileData.split(' ');
        var wordsCounter = words.length*4-16; 

        // Insert words
        var allPairs = []
        for (var i=2; i<(words.length-2); i++) {

            // Setup buff pairs
            var buffPairs = [{
                a: words[i],
                b: words[i-2]
            }, {
                a: words[i],
                b: words[i-1]
            }, {
                a: words[i],
                b: words[i+1]
            }, {
                a: words[i],
                b: words[i+2]
            }];

            allPairs = allPairs.concat(buffPairs);
        }

        var f = function (pairsList) {
            if (pairsList.length) {
                var buffPair = pairsList.shift();
                // console.log('Inserting:', buffPair)
                console.log('Advanced', Math.round((wordsCounter-pairsList.length)/pairsList.length*10000)/100+'%')

                // Search if pair exists in db
                bigrams.findOne(buffPair).then(function (pairFromDb) {

                    // Create pair in db if not created before
                    if (!pairFromDb) {
                        buffPair.count = 1;
                        bigrams.insert(buffPair).then(function () {
                            f(pairsList);
                        }).catch(function () {
                            f(pairsList);
                        })
                        return;
                    }

                    // Update pair count
                    pairFromDb.count++;
                    bigrams.updateOne(
                        { _id: pairFromDb._id },
                        pairFromDb
                    ).then(function () {
                        f(pairsList);
                    }).catch(function () {
                        f(pairsList);
                    });

                });
                
            } else {
                // Close connection
                deferred.resolve();
                db.close();
            }
        }

        f(allPairs);

        // allPairs.forEach(function (pair, j) {

        //     // Search if pair exists in db
        //     bigrams.findOne(pair).then(function (pairFromDb) {

        //         // Create pair in db if not created before
        //         if (!pairFromDb) {
        //             pair.count = 1;
        //             bigrams.insert(pair).then(function () {
        //             });
        //             return;
        //         }

        //         // Update pair count
        //         pairFromDb.count++;
        //         bigrams.updateOne(
        //             { _id: pairFromDb._id },
        //             pairFromDb
        //         ).then(function () {
        //         });

        //     });

        // })


    });

    return deferred.promise;
}
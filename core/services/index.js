exports.cleanTextFromFile = require('./cleanTextFromFile');
exports.fromPdfsToFile = require('./fromPdfsToFile');
exports.fromFileToDb = require('./fromFileToDb');
exports.getSentences = require('./getSentences');